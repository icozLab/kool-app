import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './layout/login/login.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './layout/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LeftMenuComponent } from './layout/left-menu/left-menu.component';
import { authInterceptorProviders } from './helpers/auth.interceptor';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { TokenStorageService } from './services/token-storage.service';
import { PlatComponent } from './layout/plat/plat.component';
import { PlatService } from './services/plat.service';
import { UpdatePlatComponent } from './layout/update-plat/update-plat.component';
import { PrestataireService } from './services/prestataire.service';
import { MenuListComponent } from './layout/menu/menu-list/menu-list.component';
import { AddMenuComponent } from './layout/menu/add-menu/add-menu.component';
import { MenuService } from './services/menu.service';
import { ReservationListComponent } from './layout/reservation/reservation-list/reservation-list.component';
import { ShowReservationComponent } from './layout/reservation/show-reservation/show-reservation.component';
import { AddPlatComponent } from './layout/plats/add-plat/add-plat.component';
import { AllergeneListComponent } from './layout/allergene/allergene-list/allergene-list.component';
import { AddAllergeneComponent } from './layout/allergene/add-allergene/add-allergene.component';
import { UpdateAllergeneComponent } from './layout/allergene/update-allergene/update-allergene.component';
import { UpdateGammeComponent } from './layout/gamme/update-gamme/update-gamme.component';
import { AddGammeComponent } from './layout/gamme/add-gamme/add-gamme.component';
import { GammeListComponent } from './layout/gamme/gamme-list/gamme-list.component';
import { NutritionListComponent } from './layout/nutrition/nutrition-list/nutrition-list.component';
import { AddNutritionComponent } from './layout/nutrition/add-nutrition/add-nutrition.component';
import { UpdateNutritionComponent } from './layout/nutrition/update-nutrition/update-nutrition.component';
import { UpdatePortionComponent } from './layout/portion/update-portion/update-portion.component';
import { PortionListComponent } from './layout/portion/portion-list/portion-list.component';
import { AddPortionComponent } from './layout/portion/add-portion/add-portion.component';
import { AddPrestataireComponent } from './layout/prestataire/add-prestataire/add-prestataire.component';
import { UpdatePrestataireComponent } from './layout/prestataire/update-prestataire/update-prestataire.component';
import { UpdateThemeComponent } from './layout/theme/update-theme/update-theme.component';
import { ThemeListComponent } from './layout/theme/theme-list/theme-list.component';
import { AddThemeComponent } from './layout/theme/add-theme/add-theme.component';
import { AddTypePlatComponent } from './layout/typePlat/add-type-plat/add-type-plat.component';
import { UpdateTypePlatComponent } from './layout/typePlat/update-type-plat/update-type-plat.component';
import { TypePlatListComponent } from './layout/typePlat/type-plat-list/type-plat-list.component';
import { RestaurantListComponent } from './layout/restaurant/restaurant-list/restaurant-list.component';
import { AddRestaurantComponent } from './layout/restaurant/add-restaurant/add-restaurant.component';
import { UpdateRestaurantComponent } from './layout/restaurant/update-restaurant/update-restaurant.component';
import { PrestaireListComponent } from './layout/prestataire/prestataire-list/prestataire-list.component';
import { GammeServiceService } from './services/gamme-service.service';
import { ThemeService } from './services/theme.service';
import { PopupService } from '@ng-bootstrap/ng-bootstrap/util/popup';
import { PortionService } from './services/portion.service';
import { AllergeneService } from './services/allergene.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FooterComponent,
    HomeComponent,
    LeftMenuComponent,
    PlatComponent,
    UpdatePlatComponent,
    MenuListComponent,
    AddMenuComponent,
    ReservationListComponent,
    ShowReservationComponent,
    AddPlatComponent,
    AllergeneListComponent,
    AddAllergeneComponent,
    UpdateAllergeneComponent,
    UpdateGammeComponent,
    AddGammeComponent,
    GammeListComponent,
    NutritionListComponent,
    AddNutritionComponent,
    UpdateNutritionComponent,
    UpdatePortionComponent,
    PortionListComponent,
    AddPortionComponent,
    AddPrestataireComponent,
    AddPrestataireComponent,
    UpdatePrestataireComponent,
    UpdateThemeComponent,
    ThemeListComponent,
    AddThemeComponent,
    AddTypePlatComponent,
    UpdateTypePlatComponent,
    TypePlatListComponent,
    RestaurantListComponent,
    AddRestaurantComponent,
    UpdateRestaurantComponent,
    PrestaireListComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [authInterceptorProviders,
    AuthService,
    UserService,
    TokenStorageService,
    PlatService,
    PrestataireService,
    MenuService,
    GammeServiceService,
    ThemeService,
    PortionService,
    AllergeneService],
  bootstrap: [AppComponent]
})
export class AppModule { }
