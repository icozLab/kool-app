
export class Plat {
    constructor(
        public id:string,
        public cal: string,
        public description: string,
        public image: string,
        public nom: string,
        public risque: string,
        public prestataire: string
    ) {

    }
}
