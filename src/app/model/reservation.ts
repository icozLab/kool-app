import { Restaurant } from './restaurant';
import { User } from './user';
import { Plat } from './plat';

export class Reservation {
    constructor(
        public id: string,
        public user: User,
        public restaurant: Restaurant,
        public code: string,
        public valie: string,
        public date: string,
        public created: string,
        public plats:Plat[]
    ) { }
}
