export class Allergene {
    constructor(
        public name: string,
        public id?: number
    ) { }
}
