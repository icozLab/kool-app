export class Portion {
    constructor(
        public name: string,
        public id?: number
    ) { }
}
