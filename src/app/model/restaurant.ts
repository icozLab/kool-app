export class Restaurant {
    constructor(
        public id:string,
        public nom: string,
        public adresse: string,
        public restaurant:string
    ) {}
}
