import { Restaurant } from './restaurant';
import { Theme } from './theme';
import { User } from './user';

export class Room {
    constructor(
        public id: number,
        public user: User,
        public niveau: string,
        public titre: string,
        public description: string,
        public lieu: string,
        public date: string,
        public heure: string,
        public duree: string,
        public valid: string,
        public nombrePersonne: string,
        public created: string,
        public theme: Theme,
        public restaurant: Restaurant
    ) {

    }
}
