export class Prestaire {
    constructor(public name: string,
        public responsable: string,
        public telephone: string,
        public mail: string,
        public adresse: string,
        public description: string) {

    }
}
