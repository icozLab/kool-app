export class Menu {
    constructor(
        public id:string,
        public nom: string,
        public date: string,
        public restaurant:string
    ) {}
}