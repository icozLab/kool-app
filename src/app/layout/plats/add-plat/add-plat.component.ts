import { Component, OnInit } from '@angular/core';
import { Allergene } from 'src/app/model/allergene';
import { Gamme } from 'src/app/model/gamme';
import { Prestaire } from 'src/app/model/prestaire';
import { TypePlat } from 'src/app/model/type-plat';
import { AllergeneService } from 'src/app/services/allergene.service';
import { GammeServiceService } from 'src/app/services/gamme-service.service';
import { PlatService } from 'src/app/services/plat.service';
import { PrestataireService } from 'src/app/services/prestataire.service';
import { TypePlatService } from 'src/app/services/type-plat.service';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';


@Component({
  selector: 'app-add-plat',
  templateUrl: './add-plat.component.html',
  styleUrls: ['./add-plat.component.css']
})
export class AddPlatComponent implements OnInit {
  typePlats: TypePlat[];
  gammes: Gamme[];
  prestataires:Prestaire[];
  allergenes:Allergene[];
  pinForm: FormGroup;


  constructor(private platService: PlatService,
    private typePlatService: TypePlatService,
    private gammeService: GammeServiceService,
    private prestataireService:PrestataireService,
    private formBuilder: FormBuilder,
    private allergenService:AllergeneService) {

  }

  ngOnInit() {
    this.typePlatService.readTypePlats().subscribe(
      response => {
        this.typePlats = response;
      }
    );
    this.gammeService.readGammes().subscribe(
      response => {
        this.gammes = response;
      }
    );

    this.prestataireService.readPrestataires().subscribe(
      response => {
        this.prestataires = response;
      }
    );

    this.allergenService.readAllergenes().subscribe(
      response => {
        this.allergenes = response;
      }
    );

    this.pinForm = this.formBuilder.group({
      nom: ['', [Validators.required, Validators.minLength(5)]],
      calorie: ['', [Validators.required, Validators.minLength(5)]],
      descriptif: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.pinForm.invalid) {
      return;
    } else {
      this.platService.createPlat(this.pinForm.value)
    }
  }
}

