import { Component, OnInit } from '@angular/core';
import { ReservationService } from 'src/app/services/reservation.service';
import { Router } from '@angular/router';
import { Reservation } from 'src/app/model/reservation';

@Component({
  selector: 'app-show-reservation',
  templateUrl: './show-reservation.component.html',
  styleUrls: ['./show-reservation.component.css']
})
export class ShowReservationComponent implements OnInit {

  reservation: Reservation;

  constructor(private reservationService: ReservationService, private router: Router) { }

  ngOnInit() {
    let reservationId = window.localStorage.getItem("showReservationId");
    this.reservationService.findReservationById(reservationId).subscribe(response => {
      this.reservation = response;
    })
  }

}
