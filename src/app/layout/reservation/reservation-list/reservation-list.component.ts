import { Component, OnInit } from '@angular/core';
import { Reservation } from 'src/app/model/reservation';
import { ReservationService } from 'src/app/services/reservation.service';
import { Router } from '@angular/router';
import { Restaurant } from 'src/app/model/restaurant';
import { RestaurantService } from 'src/app/services/restaurant.service';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {
  reservations: Reservation[];
  restaurants: Restaurant[];
  

  constructor(private reservationService: ReservationService, private restaurantService: RestaurantService, private router: Router) {

  }

  ngOnInit() {
    this.reservationService.readReservations().subscribe(
      response => {
        this.reservations = response;
      }
    );

    this.restaurantService.readRestaurants().subscribe(
      response => {
        this.restaurants = response;
      }
    );
  }

  showReservation(reservation: Reservation): void {
    window.localStorage.removeItem("showReservationId");
    window.localStorage.setItem("showReservationId", reservation.id.toString());
    this.router.navigate(['showReservation']);
  };

  deleteReservation(reservation: Reservation): void {
    this.reservationService.deleteReservation(reservation);
  }


}
