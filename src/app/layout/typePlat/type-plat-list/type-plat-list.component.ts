import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TypePlat } from 'src/app/model/type-plat';
import { TypePlatService } from 'src/app/services/type-plat.service';

@Component({
  selector: 'app-type-plat-list',
  templateUrl: './type-plat-list.component.html',
  styleUrls: ['./type-plat-list.component.css']
})
export class TypePlatListComponent implements OnInit {

  categories: TypePlat[];

  constructor(private typePlatervice: TypePlatService, private router: Router) { }

  ngOnInit() {
    this.typePlatervice.readTypePlats().subscribe(
      response => {
        this.categories = response;
      }
    );
  }

  editTypePlat(TypePlat: TypePlat): void {
    window.localStorage.removeItem("typePlat");
    window.localStorage.setItem("typePlat", JSON.stringify(TypePlat));
    this.router.navigate(['updateTypePlat']);
  };

  showCategorie(TypePlat: TypePlat): void {
    window.localStorage.removeItem("categorieId");
    window.localStorage.setItem("categorieId", TypePlat.id.toString());
    this.router.navigate(['showCategorie']);
  };

  deleteCategorie(TypePlat: TypePlat): void {
    this.typePlatervice.deleteTypePlat(TypePlat);
    this.router.navigate(['typePlat']);

  }
}
