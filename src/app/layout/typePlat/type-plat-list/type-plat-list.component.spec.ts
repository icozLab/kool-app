import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypePlatListComponent } from './type-plat-list.component';

describe('TypePlatListComponent', () => {
  let component: TypePlatListComponent;
  let fixture: ComponentFixture<TypePlatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypePlatListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypePlatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
