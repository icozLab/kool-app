import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TypePlat } from 'src/app/model/type-plat';
import { TypePlatService } from 'src/app/services/type-plat.service';

@Component({
  selector: 'app-add-type-plat',
  templateUrl: './add-type-plat.component.html',
  styleUrls: ['./add-type-plat.component.css']
})
export class AddTypePlatComponent implements OnInit {
  typePlatForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private TypePlatService: TypePlatService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.typePlatForm = this.formBuilder.group({
      name: ''
    });
  }

  onSubmitForm() {
    const formValue = this.typePlatForm.value;
    const typePlat = new TypePlat(formValue['name']);
    this.TypePlatService.createTypePlat(typePlat).subscribe( (value) => {
      console.log( value);
    },
    (error) => {
      console.log('Uh-oh, an error occurred! : ' + error);
    },
    () => {
      console.log('Observable complete!');
    });
    window.location.href = "./typePlat";
  }

}
