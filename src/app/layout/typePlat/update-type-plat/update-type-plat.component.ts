import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TypePlat } from 'src/app/model/type-plat';
import { TypePlatService } from 'src/app/services/type-plat.service';

@Component({
  selector: 'app-update-type-plat',
  templateUrl: './update-type-plat.component.html',
  styleUrls: ['./update-type-plat.component.css']
})
export class UpdateTypePlatComponent implements OnInit {
  typePlat: TypePlat;
  typePlatForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private typePlatService: TypePlatService) {

  }

  ngOnInit() {

    this.typePlat = JSON.parse(window.localStorage.getItem('typePlat'));

    if (!this.typePlat) {
      this.router.navigate(['typePlat']);
      return;
    }
    this.typePlatForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required]
    });
    
    this.typePlatForm.setValue(this.typePlat);
  }

  onSubmitForm() {
    this.typePlatService.updtePlat(this.typePlatForm.value)
      .subscribe(
        data => {
          this.router.navigate(['typePlat']);
        },
        error => {
        });
  }

}
