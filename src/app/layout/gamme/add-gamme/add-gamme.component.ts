import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Gamme } from 'src/app/model/gamme';
import { GammeServiceService } from 'src/app/services/gamme-service.service';

@Component({
  selector: 'app-add-gamme',
  templateUrl: './add-gamme.component.html',
  styleUrls: ['./add-gamme.component.css']
})
export class AddGammeComponent implements OnInit {
  gammeForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private gammeService: GammeServiceService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.gammeForm = this.formBuilder.group({
      name: ''
    });
  }

  onSubmitForm() {
    const formValue = this.gammeForm.value;
    const gamme = new Gamme(formValue['name']);
    this.gammeService.createGamme(gamme).subscribe( (value) => {
      console.log( value);
      value;
    },
    (error) => {
      console.log('Uh-oh, an error occurred! : ' + error);
    },
    () => {
      console.log('Observable complete!');
    });
    window.location.href = "./theme";
  }

}
