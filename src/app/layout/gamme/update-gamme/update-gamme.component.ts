import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Gamme } from 'src/app/model/gamme';
import { GammeServiceService } from 'src/app/services/gamme-service.service';

@Component({
  selector: 'app-update-gamme',
  templateUrl: './update-gamme.component.html',
  styleUrls: ['./update-gamme.component.css']
})
export class UpdateGammeComponent implements OnInit {

  Gamme: Gamme;
  gammeForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private GammeService: GammeServiceService) {

  }

  ngOnInit() {

    this.Gamme = JSON.parse(window.localStorage.getItem('gamme'));

    if (!this.Gamme) {
      this.router.navigate(['gamme']);
      return;
    }
    this.gammeForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required]
    });
    
    this.gammeForm.setValue(this.Gamme);
  }

  onSubmitForm() {
    this.GammeService.updteGamme(this.gammeForm.value)
      .subscribe(
        data => {
          this.router.navigate(['gamme']);
        },
        error => {
        });
  }

}
