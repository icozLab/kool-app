import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGammeComponent } from './update-gamme.component';

describe('UpdateGammeComponent', () => {
  let component: UpdateGammeComponent;
  let fixture: ComponentFixture<UpdateGammeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGammeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
