import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Gamme } from 'src/app/model/gamme';
import { GammeServiceService } from 'src/app/services/gamme-service.service';

@Component({
  selector: 'app-gamme-list',
  templateUrl: './gamme-list.component.html',
  styleUrls: ['./gamme-list.component.css']
})
export class GammeListComponent implements OnInit {

  gammes: Gamme[];

  constructor(private gammeService: GammeServiceService, private router: Router) { }

  ngOnInit() {
    this.gammeService.readGammes().subscribe(
      response => {
        this.gammes = response;
      }
    );
  }

  editGamme(gamme: Gamme): void {
    window.localStorage.removeItem("gamme");
    window.localStorage.setItem("gamme", JSON.stringify(gamme));
    this.router.navigate(['updateGamme']);
  };

  showGamme(gamme: Gamme): void {
    window.localStorage.removeItem("gammeId");
    window.localStorage.setItem("gammeId", gamme.id.toString());
    this.router.navigate(['showGamme']);
  };

  deleteGamme(gamme: Gamme): void {
    this.gammeService.deleteGamme(gamme);
    this.router.navigate(['gamme']);
  }
}
