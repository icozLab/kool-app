import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Theme } from 'src/app/model/theme';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
  selector: 'app-theme-list',
  templateUrl: './theme-list.component.html',
  styleUrls: ['./theme-list.component.css']
})
export class ThemeListComponent implements OnInit {

  themes: Theme[];

  constructor(private themeService: ThemeService, private router: Router) { }

  ngOnInit() {
    this.themeService.readThemes().subscribe(
      response => {
        this.themes = response;
      }
    );
  }

  editTheme(theme: Theme): void {
    window.localStorage.removeItem("theme");
    window.localStorage.setItem("theme", JSON.stringify(theme));
    this.router.navigate(['updateTheme']);
  };

  showTheme(theme: Theme): void {
    window.localStorage.removeItem("themeId");
    window.localStorage.setItem("themeId", theme.id.toString());
    this.router.navigate(['showTheme']);
  };

  deleteTheme(theme: Theme): void {
    this.themeService.deleteTheme(theme);
    this.router.navigate(['theme']);

  }
}
