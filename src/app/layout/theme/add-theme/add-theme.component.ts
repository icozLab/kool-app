import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Theme } from 'src/app/model/theme';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
  selector: 'app-add-theme',
  templateUrl: './add-theme.component.html',
  styleUrls: ['./add-theme.component.css']
})
export class AddThemeComponent implements OnInit {
  themeForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private themeService: ThemeService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.themeForm = this.formBuilder.group({
      name: ''
    });
  }

  onSubmitForm() {
    const formValue = this.themeForm.value;
    const theme = new Theme(formValue['name']);
    this.themeService.createTheme(theme).subscribe( (value) => {
      console.log( value);
      value;
    },
    (error) => {
      console.log('Uh-oh, an error occurred! : ' + error);
    },
    () => {
      console.log('Observable complete!');
    });
    window.location.href = "./theme";
  }

}
