import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Theme } from 'src/app/model/theme';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
  selector: 'app-update-theme',
  templateUrl: './update-theme.component.html',
  styleUrls: ['./update-theme.component.css']
})
export class UpdateThemeComponent implements OnInit {

  theme: Theme;
  themeForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private themeService: ThemeService) {

  }

  ngOnInit() {

    this.theme = JSON.parse(window.localStorage.getItem('theme'));

    if (!this.theme) {
      alert("Invalid action.")
      this.router.navigate(['theme']);
      return;
    }
    this.themeForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required]
    });
    
    this.themeForm.setValue(this.theme);
  }

  onSubmitForm() {
    this.themeService.updateTheme(this.themeForm.value)
      .subscribe(
        data => {
          this.router.navigate(['theme']);
        },
        error => {
        });
  }

}
