import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Allergene } from 'src/app/model/allergene';
import { AllergeneService } from 'src/app/services/allergene.service';

@Component({
  selector: 'app-allergene-list',
  templateUrl: './allergene-list.component.html',
  styleUrls: ['./allergene-list.component.css']
})
export class AllergeneListComponent implements OnInit {
  allergenes: Allergene[];

  constructor(private allergeneService: AllergeneService, private router: Router) { }

  ngOnInit() {
    this.allergeneService.readAllergenes().subscribe(
      response => {
        this.allergenes = response;
      }
    );
  }

  editAllergene(allergene: Allergene): void {
    window.localStorage.removeItem("allergene");
    window.localStorage.setItem("allergene", JSON.stringify(allergene));
    this.router.navigate(['updateAllergene']);
  };

  showAllergene(allergene: Allergene): void {
    window.localStorage.removeItem("allergeneId");
    window.localStorage.setItem("allergeneId", allergene.id.toString());
    this.router.navigate(['showAllergene']);
  };

  deleteAllergene(allergene: Allergene): void {
    this.allergeneService.deleteAllergene(allergene);
    this.router.navigate(['allergene']);

  }
}
