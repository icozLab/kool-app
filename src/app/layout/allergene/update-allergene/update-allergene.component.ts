import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Allergene } from 'src/app/model/allergene';
import { AllergeneService } from 'src/app/services/allergene.service';

@Component({
  selector: 'app-update-allergene',
  templateUrl: './update-allergene.component.html',
  styleUrls: ['./update-allergene.component.css']
})
export class UpdateAllergeneComponent implements OnInit {

  allergene: Allergene;
  allergeneForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private AllergeneService: AllergeneService) {

  }

  ngOnInit() {

    this.allergene = JSON.parse(window.localStorage.getItem('allergene'));

    if (!this.allergene) {
      this.router.navigate(['allergene']);
      return;
    }
    this.allergeneForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required]
    });
    
    this.allergeneForm.setValue(this.allergene);
  }

  onSubmitForm() {
    this.AllergeneService.updteAllergene(this.allergeneForm.value)
      .subscribe(
        data => {
          this.router.navigate(['allergene']);
        },
        error => {
        });
  }

}
