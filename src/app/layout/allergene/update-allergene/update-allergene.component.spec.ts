import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAllergeneComponent } from './update-allergene.component';

describe('UpdateAllergeneComponent', () => {
  let component: UpdateAllergeneComponent;
  let fixture: ComponentFixture<UpdateAllergeneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAllergeneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAllergeneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
