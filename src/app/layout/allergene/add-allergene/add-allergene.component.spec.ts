import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAllergeneComponent } from './add-allergene.component';

describe('AddAllergeneComponent', () => {
  let component: AddAllergeneComponent;
  let fixture: ComponentFixture<AddAllergeneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAllergeneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAllergeneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
