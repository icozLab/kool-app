import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Allergene } from 'src/app/model/allergene';
import { AllergeneService } from 'src/app/services/allergene.service';

@Component({
  selector: 'app-add-allergene',
  templateUrl: './add-allergene.component.html',
  styleUrls: ['./add-allergene.component.css']
})
export class AddAllergeneComponent implements OnInit {
  allergeneForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private allergeneService: AllergeneService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.allergeneForm = this.formBuilder.group({
      name: ''
    });
  }

  onSubmitForm() {
    const formValue = this.allergeneForm.value;
    const allergene = new Allergene(formValue['name']);
    debugger
    this.allergeneService.createAllergene(allergene).subscribe( (value) => {
      console.log( value);
      value;
    },
    (error) => {
      console.log('Uh-oh, an error occurred! : ' + error);
    },
    () => {
      console.log('Observable complete!');
    });
    window.location.href = "./allergene";
  }

}
