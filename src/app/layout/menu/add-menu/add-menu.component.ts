import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/model/menu';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MenuService } from 'src/app/services/menu.service';
import { Router } from '@angular/router';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { Restaurant } from 'src/app/model/restaurant';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.css']
})
export class AddMenuComponent implements OnInit {

  platForm: FormGroup;
  menu: Menu;
  menus: any;
  restauran: string;
  menuId: string;
  restaurants: Restaurant[];

  constructor(private formBuilder: FormBuilder, private menuService: MenuService, public restaurantService: RestaurantService, private router: Router) { }

  ngOnInit() {
    this.restaurantService.readRestaurants().subscribe(
      response => {
        this.restaurants = response;
      }
    );

    this.initForm();

  }


  initForm() {
    this.platForm = this.formBuilder.group({
      nom: ['', Validators.required],
      date: ['', Validators.required],
      restaurant: ['', Validators.required]

    });
  }

  onSubmitForm() {
    const formValue = this.platForm.value;
    const menu = new Menu(
      formValue['id'],
      formValue['nom'],
      formValue['date'],
      this.getRestaurantByName(this.restauran)
    );
    this.menuService.createMenu(menu).subscribe(
      (value) => {
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      }
    );
    this.router.navigate(['./menu']);

  }

  getRestaurantByName(name: string): string {
   /*  this.restaurantService.g(name).subscribe(

      (value) => {
        this.menuId = value.id;
      },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      }); */
    return "this.menuId";
  }

}

