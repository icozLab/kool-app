import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/model/menu';
import { MenuService } from 'src/app/services/menu.service';
import { Router } from '@angular/router';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { Restaurant } from 'src/app/model/restaurant';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {
  menus: Menu[];
  restaurants: Restaurant[];

  constructor(private menuService: MenuService, private restaurantService: RestaurantService, private router: Router) { }

  ngOnInit() {
    this.menuService.readMenus().subscribe(
      response => {
        this.menus = response;
      }
    );
    this.restaurantService.readRestaurants().subscribe(
      response => {
        this.restaurants = response;
      }
    );
  }


  editMenu(menu: Menu): void {
    window.localStorage.removeItem("editPlatId");
    window.localStorage.setItem("editPlatId", menu.id.toString());
    this.router.navigate(['updateMenu']);
  };

}
