import { Component, OnInit } from '@angular/core';
import { Plat } from 'src/app/model/plat';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PlatService } from 'src/app/services/plat.service';

@Component({
  selector: 'app-update-plat',
  templateUrl: './update-plat.component.html',
  styleUrls: ['./update-plat.component.css']
})
export class UpdatePlatComponent implements OnInit {

  plat: Plat;
  platForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private platService: PlatService) { }

  ngOnInit() {
    let platId = window.localStorage.getItem("editPlatId");
    if (!platId) {
      alert("Invalid action.")
      this.router.navigate(['plat']);
      return;
    }
    this.platForm = this.formBuilder.group({
      id: [''],
      nom: ['', Validators.required],
      image: ['', Validators.required],
      risque: ['', Validators.required],
      cal: ['', Validators.required],
      description: ['', Validators.required],
      prestataire: ['', Validators.required]
    });
    this.platService.getPlatById(platId)
      .subscribe(
        (value) => {this.platForm.setValue(value); },
        (error) => {console.log('Uh-oh, an error occurred! : ' + error);},
        () => {console.log('Observable complete!');}
      );
  }

  onSubmitForm() {
    this.platService.updtePlat(this.platForm.value)
      //.pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['plat']);
        // if (data.status === 200) {
          //  alert('User updated successfully.');
            this.router.navigate(['plat']);
        //  } else {
         //   alert(data.message);
        //  }
        },
        error => {
          alert(error);
        });
  }

}
