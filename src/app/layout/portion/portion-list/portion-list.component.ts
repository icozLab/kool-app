import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Portion } from 'src/app/model/portion';
import { PortionService } from 'src/app/services/portion.service';

@Component({
  selector: 'app-portion-list',
  templateUrl: './portion-list.component.html',
  styleUrls: ['./portion-list.component.css']
})
export class PortionListComponent implements OnInit {

  portions: Portion[];

  constructor(private portionService: PortionService, private router: Router) { }

  ngOnInit() {
    this.portionService.readPortions().subscribe(
      response => {
        this.portions = response;
      }
    );
  }

  editPortion(portion: Portion): void {
    window.localStorage.removeItem("portion");
    window.localStorage.setItem("portion", JSON.stringify(portion));
    this.router.navigate(['updatePortion']);
  };

  showPortion(portion: Portion): void {
    window.localStorage.removeItem("portionId");
    window.localStorage.setItem("portionId", portion.id.toString());
    this.router.navigate(['showPortion']);
  };

  deletePortion(portion: Portion): void {
    this.portionService.deletePortion(portion);
    this.router.navigate(['portion']);

  }
}