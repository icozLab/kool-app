import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePortionComponent } from './update-portion.component';

describe('UpdatePortionComponent', () => {
  let component: UpdatePortionComponent;
  let fixture: ComponentFixture<UpdatePortionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePortionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePortionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
