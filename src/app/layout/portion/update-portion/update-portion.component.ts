import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Portion } from 'src/app/model/portion';
import { PortionService } from 'src/app/services/portion.service';

@Component({
  selector: 'app-update-portion',
  templateUrl: './update-portion.component.html',
  styleUrls: ['./update-portion.component.css']
})
export class UpdatePortionComponent implements OnInit {
  portion: Portion;
  portionForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private portionService: PortionService) {

  }

  ngOnInit() {

    this.portion = JSON.parse(window.localStorage.getItem('portion'));

    if (!this.portion) {
      this.router.navigate(['portion']);
      return;
    }
    this.portionForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required]
    });
    
    this.portionForm.setValue(this.portion);
  }

  onSubmitForm() {
    this.portionService.updtePortion(this.portionForm.value)
      .subscribe(
        data => {
          this.router.navigate(['portion']);
        },
        error => {
        });
  }

}
