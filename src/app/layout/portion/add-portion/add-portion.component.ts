import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Portion } from 'src/app/model/portion';
import { PortionService } from 'src/app/services/portion.service';

@Component({
  selector: 'app-add-portion',
  templateUrl: './add-portion.component.html',
  styleUrls: ['./add-portion.component.css']
})
export class AddPortionComponent implements OnInit {
  portionForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private portionService: PortionService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.portionForm = this.formBuilder.group({
      name: ''
    });
  }

  onSubmitForm() {
    const formValue = this.portionForm.value;
    const portion = new Portion(formValue['name']);
    this.portionService.createPortion(portion).subscribe( (value) => {
      console.log( value);
    },
    (error) => {
      console.log('Uh-oh, an error occurred! : ' + error);
    },
    () => {
      console.log('Observable complete!');
    });
    window.location.href = "./portion";
  }

}
