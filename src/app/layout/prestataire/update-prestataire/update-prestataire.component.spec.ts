import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePrestataireComponent } from './update-prestataire.component';

describe('UpdatePrestataireComponent', () => {
  let component: UpdatePrestataireComponent;
  let fixture: ComponentFixture<UpdatePrestataireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePrestataireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePrestataireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
