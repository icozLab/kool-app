import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Prestaire } from 'src/app/model/prestaire';
import { PrestataireService } from 'src/app/services/prestataire.service';

@Component({
  selector: 'app-update-prestataire',
  templateUrl: './update-prestataire.component.html',
  styleUrls: ['./update-prestataire.component.css']
})
export class UpdatePrestataireComponent implements OnInit {

  prestataire: Prestaire;
  prestataireForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private prestataireService: PrestataireService) {

  }

  ngOnInit() {

    this.prestataire = JSON.parse(window.localStorage.getItem('prestataire'));

    if (!this.prestataire) {
      this.router.navigate(['prestataire']);
      return;
    }
    this.prestataireForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      responsable: ['', Validators.required],
      telephone: ['', Validators.required],
      mail: ['', Validators.required],
      adresse: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.prestataireForm.setValue(this.prestataire);
  }

  onSubmitForm() {
    this.prestataireService.updatePrestataire(this.prestataireForm.value)
      .subscribe(
        data => {
          this.router.navigate(['prestataire']);
        },
        error => {
        });
  }

}
