import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Prestaire } from 'src/app/model/prestaire';
import { PrestataireService } from 'src/app/services/prestataire.service';

@Component({
  selector: 'app-add-prestataire',
  templateUrl: './add-prestataire.component.html',
  styleUrls: ['./add-prestataire.component.css']
})
export class AddPrestataireComponent implements OnInit {
  prestataireForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private prestataireService: PrestataireService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.prestataireForm = this.formBuilder.group({
      name: '',
      responsable: '',
      mail: '',
      adresse: '',
      telephone: '',
      description: ''
    });
  }

  onSubmitForm() {
    const formValue = this.prestataireForm.value;
    const prestaire = new Prestaire(formValue['name'],
      formValue['responsable'] ,
      formValue['telephone'],
      formValue['mail'],
      formValue['adresse'],
      formValue['description'] 
    );
    debugger
    this.prestataireService.createPrestataire(prestaire).subscribe((value) => {
      console.log(value);
      value;
    },
      (error) => {
        console.log('Uh-oh, an error occurred! : ' + error);
      },
      () => {
        console.log('Observable complete!');
      });
    window.location.href = "./prestataire";
  }

}
