import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Prestaire } from 'src/app/model/prestaire';
import { PrestataireService } from 'src/app/services/prestataire.service';

@Component({
  selector: 'app-prestataire-list',
  templateUrl: './prestataire-list.component.html',
  styleUrls: ['./prestataire-list.component.css']
})
export class PrestaireListComponent implements OnInit {

  prestataires: Prestaire[];

  constructor(private prestataireService: PrestataireService, private router: Router) { }

  ngOnInit() {
    this.prestataireService.readPrestataires().subscribe(
      response => {
        this.prestataires = response;
      }
    );
  }

  editPrestataire(prestataire: Prestaire): void {
    window.localStorage.removeItem("prestataire");
    window.localStorage.setItem("prestataire", JSON.stringify(prestataire));
    this.router.navigate(['updatePrestataire']);
  };

  showPrestaire(prestataire: Prestaire): void {
    window.localStorage.removeItem("prestataireId");
    //   window.localStorage.setItem("prestataireId", prestataire.id.toString());
    this.router.navigate(['showPrestaire']);
  };

  deletePrestaire(prestataire: Prestaire): void {
    this.prestataireService.deletePrestataire(prestataire);
    this.router.navigate(['prestataire']);

  }
}