import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Nutrition } from 'src/app/model/nutrition';
import { NutritionService } from 'src/app/services/nutrition.service';

@Component({
  selector: 'app-update-nutrition',
  templateUrl: './update-nutrition.component.html',
  styleUrls: ['./update-nutrition.component.css']
})
export class UpdateNutritionComponent implements OnInit {

  nutrition: Nutrition;
  nutritionForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private NutritionService: NutritionService) {

  }

  ngOnInit() {

    this.nutrition = JSON.parse(window.localStorage.getItem('nutrition'));

    if (!this.nutrition) {
      this.router.navigate(['nutrition']);
      return;
    }
    this.nutritionForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required]
    });
    
    this.nutritionForm.setValue(this.nutrition);
  }

  onSubmitForm() {
    this.NutritionService.updteNutrition(this.nutritionForm.value)
      .subscribe(
        data => {
          this.router.navigate(['nutrition']);
        },
        error => {
        });
  }

}
