import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateNutritionComponent } from './update-nutrition.component';

describe('UpdateNutritionComponent', () => {
  let component: UpdateNutritionComponent;
  let fixture: ComponentFixture<UpdateNutritionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateNutritionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateNutritionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
