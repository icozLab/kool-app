import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Nutrition } from 'src/app/model/nutrition';
import { NutritionService } from 'src/app/services/nutrition.service';

@Component({
  selector: 'app-nutrition-list',
  templateUrl: './nutrition-list.component.html',
  styleUrls: ['./nutrition-list.component.css']
})
export class NutritionListComponent implements OnInit {

  nutritions: Nutrition[];

  constructor(private nutritionService: NutritionService, private router: Router) { }

  ngOnInit() {
    this.nutritionService.readNutritions().subscribe(
      response => {
        this.nutritions = response;
      }
    );
  }

  editNutrition(nutrition: Nutrition): void {
    window.localStorage.removeItem("nutrition");
    window.localStorage.setItem("nutrition", JSON.stringify(nutrition));
    this.router.navigate(['updateNutrition']);
  };

  showNutrition(nutrition: Nutrition): void {
    window.localStorage.removeItem("nutritionId");
    window.localStorage.setItem("nutritionId", nutrition.id.toString());
    this.router.navigate(['showNutrition']);
  };

  deleteNutrition(nutrition: Nutrition): void {
    this.nutritionService.deleteNutrition(nutrition);
    this.router.navigate(['nutrition']);

  }
}