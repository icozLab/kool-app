import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Nutrition } from 'src/app/model/nutrition';
import { NutritionService } from 'src/app/services/nutrition.service';

@Component({
  selector: 'app-add-nutrition',
  templateUrl: './add-nutrition.component.html',
  styleUrls: ['./add-nutrition.component.css']
})
export class AddNutritionComponent implements OnInit {
  nutritionForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private nutritionService: NutritionService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.nutritionForm = this.formBuilder.group({
      name: ''
    });
  }

  onSubmitForm() {
    const formValue = this.nutritionForm.value;
    const nutrition = new Nutrition(formValue['name']);
    this.nutritionService.createNutrition(nutrition).subscribe( (value) => {
      console.log( value);
      value;
    },
    (error) => {
      console.log('Uh-oh, an error occurred! : ' + error);
    },
    () => {
      console.log('Observable complete!');
    });
    window.location.href = "./nutrition";
  }

}
