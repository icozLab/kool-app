import { Component, OnInit } from '@angular/core';
import { PlatService } from 'src/app/services/plat.service';
import { Plat } from 'src/app/model/plat';
import { Router } from '@angular/router';
import { Prestaire } from 'src/app/model/prestaire';
import { PrestataireService } from 'src/app/services/prestataire.service';
import { TypePlatService } from 'src/app/services/type-plat.service';
import { TypePlat } from 'src/app/model/type-plat';

@Component({
  selector: 'app-plat',
  templateUrl: './plat.component.html',
  styleUrls: ['./plat.component.css']
})
export class PlatComponent implements OnInit {
  plats: Plat[];
  typePlats: TypePlat[];
  prestataires: Prestaire[];
  prestataireName: string;
  typePlatName: string;


  constructor(private platService: PlatService,
    private typePlatService: TypePlatService,
    private prestataireService: PrestataireService,
    private router: Router) { }

  ngOnInit() {
    this.platService.readPlats().subscribe(
      response => {
        this.plats = response;
      }
    );

    this.typePlatService.readTypePlats().subscribe(
      response => {
        this.typePlats = response;
      }
    );

    this.prestataireService.readPrestataires().subscribe(
      response => {
        this.prestataires = response;
      }
    );
  }

  editPlat(plat: Plat): void {
    window.localStorage.removeItem("editPlatId");
    window.localStorage.setItem("editPlatId", plat.id.toString());
    this.router.navigate(['updatePlat']);
  };

  showPlat(plat: Plat): void {
    window.localStorage.removeItem("platId");
    window.localStorage.setItem("platId", plat.id.toString());
    this.router.navigate(['showPlat']);
  };

  deletePlat(plat: Plat): void {
    this.platService.deletePlat(plat);
    this.router.navigate(['plat']);

  }

  selectPrestataire(event: any) {
    this.prestataireName = event.target.value;
    this.platService.getAllPlatByPrestataire(this.prestataireName).subscribe(
      response => {
        this.plats = response;
      }
    );
  }

  selectTypePlat(event: any) {
    this.typePlatName = event.target.value;
    this.platService.getAllPlatByTypePlat(this.typePlatName).subscribe(
      response => {
        this.plats = response;
      }
    );
  }

}
