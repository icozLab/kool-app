import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Restaurant } from 'src/app/model/restaurant';
import { RestaurantService } from 'src/app/services/restaurant.service';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.css']
})
export class RestaurantListComponent implements OnInit {
  restaurants: Restaurant[];

  constructor(private restaurantsService: RestaurantService, private router: Router) { }

  ngOnInit() {
    this.restaurantsService.readRestaurants().subscribe(
      response => {
        this.restaurants = response;
      }
    );
  }

  editRestaurant(prestataire: Restaurant): void {
    window.localStorage.removeItem("editRestaurantId");
    window.localStorage.setItem("editRestaurantId", prestataire.id.toString());
    this.router.navigate(['updateRestaurant']);
  };

  showRestaurant(prestataire: Restaurant): void {
    window.localStorage.removeItem("restaurantId");
    window.localStorage.setItem("restaurantIdId", prestataire.id.toString());
    this.router.navigate(['showRestaurant']);
  };

  deleteRestaurant(restaurant: Restaurant): void {
    this.restaurantsService.deleteRestaurant(restaurant);
  }
}

