import { TestBed } from '@angular/core/testing';

import { TypePlatService } from './type-plat.service';

describe('TypePlatService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypePlatService = TestBed.get(TypePlatService);
    expect(service).toBeTruthy();
  });
});
