import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Nutrition } from '../model/nutrition';

@Injectable({
  providedIn: 'root'
})
export class NutritionService {
  private basUrl = 'http://localhost:8080/api/v1'


  constructor(private httpClient: HttpClient) { }

  readNutritions(): Observable<Nutrition[]> {
    return this.httpClient.get<Nutrition[]>(this.basUrl + '/nutrition');

  }

  createNutrition(nutritionToCreate: Nutrition): Observable<Nutrition> {
    return this.httpClient.post<Nutrition>(this.basUrl + '/addNutrition', nutritionToCreate);
  }

  updteNutrition(nutritionToUpdate: Nutrition): Observable<Nutrition> {
    return this.httpClient.post<Nutrition>(this.basUrl + '/updateNutrition', nutritionToUpdate);
  }

  getNutritionById(nutritionId: string): Observable<Nutrition> {
    return this.httpClient.get<Nutrition>(this.basUrl + 'getNutritionById/' + nutritionId);
  }

  deleteNutrition(nutrition: Nutrition) {
    return this.httpClient.delete(this.basUrl + nutrition.id);
  }

}
