import { Injectable } from '@angular/core';
import { Menu } from '../model/menu';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  
  constructor( private httpClient: HttpClient) { }

  readMenus(): Observable<Menu[]> {
    return this.httpClient.get<Menu[]>('http://localhost:8080/api/v1/menu');

  }

  readPublishedMenus(): Observable<Menu[]> {
    return this.httpClient.get<Menu[]>('http://localhost:8080/api/v1/publishedMenu');

  }

  createMenu(menuToCreate: Menu): Observable<Menu> {
    return this.httpClient.post<Menu>('http://localhost:8080/api/v1/addMenu', menuToCreate);
  }

  publishedMenuByName(name:string): Observable<Menu> {
    return this.httpClient.post<Menu>('http://localhost:8080/api/v1/publishedMenu/',name);
  }
}
