import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Gamme } from '../model/gamme';

@Injectable({
  providedIn: 'root'
})
export class GammeServiceService {
  private basUrl = 'http://localhost:8080/api/v1/'


  constructor(private httpClient: HttpClient) { }

  readGammes(): Observable<Gamme[]> {
    return this.httpClient.get<Gamme[]>(this.basUrl + 'gamme');

  }

  createGamme(gammeToCreate: Gamme): Observable<Gamme> {
    console.log(this.basUrl + 'addGamme', gammeToCreate);
    debugger
    return this.httpClient.post<Gamme>(this.basUrl + 'addGamme', gammeToCreate);
  }

  updteGamme(gammeToUpdate: Gamme): Observable<Gamme> {
    return this.httpClient.post<Gamme>(this.basUrl + '/updateGamme', gammeToUpdate);
  }

  getGammeById(gammeId: string): Observable<Gamme> {
    return this.httpClient.get<Gamme>(this.basUrl + 'getGammeById/' + gammeId);
  }

  deleteGamme(gamme: Gamme) {
    return this.httpClient.delete(this.basUrl + gamme.id);
  }

}
