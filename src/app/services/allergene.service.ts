import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Allergene } from '../model/allergene';

@Injectable({
  providedIn: 'root'
})
export class AllergeneService {
  private basUrl = 'http://localhost:8080/api/v1'


  constructor(private httpClient: HttpClient) { }

  readAllergenes(): Observable<Allergene[]> {
    return this.httpClient.get<Allergene[]>(this.basUrl + '/allergene');

  }

  createAllergene(allergeneToCreate: Allergene): Observable<Allergene> {
    return this.httpClient.post<Allergene>(this.basUrl + '/addAllergene', allergeneToCreate);
  }

  updteAllergene(allergeneToUpdate: Allergene): Observable<Allergene> {
    return this.httpClient.post<Allergene>(this.basUrl + '/updateAllergene', allergeneToUpdate);
  }

  getAllergeneById(allergeneId: string): Observable<Allergene> {
    return this.httpClient.get<Allergene>(this.basUrl + 'getAllergeneById/' + allergeneId);
  }

  deleteAllergene(allergene: Allergene) {
    return this.httpClient.delete(this.basUrl + allergene.id);
  }

}
