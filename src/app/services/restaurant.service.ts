import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Restaurant } from '../model/restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  private basUrl = 'http://localhost:8080/api/v1/'

  constructor(private httpClient: HttpClient) { }

  readRestaurants(): Observable<Restaurant[]> {
    return this.httpClient.get<Restaurant[]>(this.basUrl + 'Restaurant');

  }

  createRestaurant(restaurantToCreate: Restaurant): Observable<Restaurant> {
    return this.httpClient.post<Restaurant>(this.basUrl + 'addRestaurant', restaurantToCreate);
  }

  deleteRestaurant(restaurant: Restaurant) {
    return this.httpClient.delete(this.basUrl + restaurant.id);
  }

  findRestaurantById(restaurantId: string): Observable<Restaurant> {
    return this.httpClient.get<Restaurant>(this.basUrl + restaurantId);
  }

}
