import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Portion } from '../model/portion';

@Injectable({
  providedIn: 'root'
})
export class PortionService {
  private basUrl = 'http://localhost:8080/api/v1'


  constructor(private httpClient: HttpClient) { }

  readPortions(): Observable<Portion[]> {
    return this.httpClient.get<Portion[]>(this.basUrl + '/portion');

  }

  createPortion(portionToCreate: Portion): Observable<Portion> {
    return this.httpClient.post<Portion>(this.basUrl + '/addPortion', portionToCreate);
  }

  updtePortion(portionToUpdate: Portion): Observable<Portion> {
    return this.httpClient.post<Portion>(this.basUrl + '/updatePortion', portionToUpdate);
  }

  getPortionById(portionId: string): Observable<Portion> {
    return this.httpClient.get<Portion>(this.basUrl + 'getPortionById/' + portionId);
  }

  deletePortion(portion: Portion) {
    return this.httpClient.delete(this.basUrl + portion.id);
  }

}
