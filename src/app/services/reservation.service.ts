import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Reservation } from '../model/reservation';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  private basUrl = 'http://localhost:8080/api/v1/'

  constructor(private httpClient: HttpClient) { }

  readReservations(): Observable<Reservation[]> {
    return this.httpClient.get<Reservation[]>(this.basUrl + 'reservation');

  }

  createReservation(reservationToCreate: Reservation): Observable<Reservation> {
    return this.httpClient.post<Reservation>(this.basUrl + 'addReservation', reservationToCreate);
  }

  deleteReservation(reservation: Reservation) {
    return this.httpClient.delete(this.basUrl + reservation.id);
  }

  findReservationById(reservationId: string): Observable<Reservation> {
    return this.httpClient.get<Reservation>(this.basUrl + reservationId);
  }

}
