import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Theme } from '../model/theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private basUrl = 'http://localhost:8080/api/v1'


  constructor(private httpClient: HttpClient) { }

  readThemes(): Observable<Theme[]> {
    return this.httpClient.get<Theme[]>(this.basUrl + '/theme');

  }

  createTheme(themeToCreate: Theme): Observable<Theme> {
    return this.httpClient.post<Theme>(this.basUrl + '/addTheme', themeToCreate);
  }

  updateTheme(themeToUpdate: Theme): Observable<Theme> {
    return this.httpClient.post<Theme>(this.basUrl + '/updateTheme', themeToUpdate);
  }

  getThemeById(themeId: string): Observable<Theme> {
    return this.httpClient.get<Theme>(this.basUrl + 'getThemeById/' + themeId);
  }

  deleteTheme(theme: Theme) {
    return this.httpClient.delete(this.basUrl + theme.id);
  }

}
