import { Injectable } from '@angular/core';
import { Prestaire } from '../model/prestaire';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PrestataireService {
  

  
  endpoint: string = 'http://localhost:8080/api/v1';

  constructor( private httpClient: HttpClient) { }

  createPrestataire(prestataireToCreate: Prestaire): Observable<Prestaire> {
    return this.httpClient.post<Prestaire>(this.endpoint+'/addPrestataire', prestataireToCreate);
  }

  updatePrestataire(prestataireToUpdate: Prestaire): Observable<Prestaire> {
    return this.httpClient.post<Prestaire>(this.endpoint+'/updatePrestataire', prestataireToUpdate);
  }

  readPrestataires(): Observable<Prestaire[]> {
    return this.httpClient.get<Prestaire[]>(this.endpoint+'/prestataire');
  }

  getPrestataireByName(name:string): Observable<Prestaire> {
    return this.httpClient.get<Prestaire>('http://localhost:8080/api/v1/getPrestataireByName/'+name);
  }
  deletePrestataire(prestaire: Prestaire) {
    //return this.httpClient.delete(this.endpoint + prestaire.id);
  }
}
