import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from '../model/room';

@Injectable({
  providedIn: 'root'
})
export class RoomService {
  basUrl = 'http://localhost:8080/api/v1/Room/';

  constructor(private httpClient: HttpClient) { }

  readRooms(): Observable<Room[]> {
    return this.httpClient.get<Room[]>(this.basUrl);

  }

  createRoom(roomToCreate: Room): Observable<Room> {
    return this.httpClient.post<Room>(this.basUrl + 'addRoom/', roomToCreate);
  }

  updteRoom(roomToUpdate: Room): Observable<Room> {
    return this.httpClient.post<Room>(this.basUrl + 'addRoom/', roomToUpdate);
  }

  getRoomById(roomId: string): Observable<Room> {
    return this.httpClient.get<Room>(this.basUrl + 'getRoomById/' + roomId);
  }

}