import { TestBed } from '@angular/core/testing';

import { GammeServiceService } from './gamme-service.service';

describe('GammeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GammeServiceService = TestBed.get(GammeServiceService);
    expect(service).toBeTruthy();
  });
});
