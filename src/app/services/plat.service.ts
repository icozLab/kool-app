import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Plat } from '../model/plat';
import { throwError } from 'rxjs/internal/observable/throwError';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlatService {

 private basUrl = 'http://localhost:8080/api/v1/'
  
  endpoint: string = '[http://localhost:8080/api/v1]';

  constructor( private httpClient: HttpClient) { }

  readPlats(): Observable<Plat[]> {
    return this.httpClient.get<Plat[]>(this.basUrl+'plat');

  }

  createPlat(platToCreate: Plat): Observable<Plat> {
    return this.httpClient.post<Plat>(this.basUrl+'addPlat/', platToCreate);
  }

  updtePlat(platToUpdate: Plat): Observable<Plat> {
    return this.httpClient.post<Plat>(this.basUrl+'addPlat/', platToUpdate);
  }

  getPlatById(platId: string) :Observable<Plat> {
    return this.httpClient.get<Plat>(this.basUrl+'getPlatById/'+platId);
  }

  deletePlat(plat: Plat) {
    return this.httpClient.delete(this.basUrl + plat.id);
  }

  getAllPlatByPrestataire(prestataireName: string){
    return this.httpClient.get<Plat[]>(this.basUrl+'getPlatByPrestataire/'+prestataireName);
  }

  getAllPlatByTypePlat(typePlatName: string) {
    return this.httpClient.get<Plat[]>(this.basUrl+'getPlatByTypePlat/'+typePlatName);
  }


  // method for displaying errors
  private handleError(errorResponse: HttpErrorResponse) {
    debugger
    if (errorResponse.error instanceof ErrorEvent) {
      console.error('Client Side Error :', errorResponse.error.message);
    } else {
      console.error('Server Side Error :', errorResponse);
    }
    return throwError('There is a problem with the service. We are notified & working on it. Please try again later.');
  }
}
