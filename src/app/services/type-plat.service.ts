import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TypePlat } from '../model/type-plat';

@Injectable({
  providedIn: 'root'
})
export class TypePlatService {
  private basUrl = 'http://localhost:8080/api/v1'
  

  constructor( private httpClient: HttpClient) { }

  readTypePlats(): Observable<TypePlat[]> {
    return this.httpClient.get<TypePlat[]>(this.basUrl+'/typePlat');

  }

  createTypePlat(typePlatToCreate: TypePlat): Observable<TypePlat> {
    return this.httpClient.post<TypePlat>(this.basUrl+'/addTypePlat', typePlatToCreate);
  }

  updtePlat(typePlatToUpdate: TypePlat): Observable<TypePlat> {
    return this.httpClient.post<TypePlat>(this.basUrl+'/updateTypePlat', typePlatToUpdate);
  }

  getTypePlatById(typePlatId: string) :Observable<TypePlat> {
    return this.httpClient.get<TypePlat>(this.basUrl+'getTypePlatById/'+typePlatId);
  }

  deleteTypePlat(typePlat: TypePlat) {
    return this.httpClient.delete(this.basUrl + typePlat.id);
  }
 
}
