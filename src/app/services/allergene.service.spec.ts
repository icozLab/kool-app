import { TestBed } from '@angular/core/testing';

import { AllergeneService } from './allergene.service';

describe('AllergeneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllergeneService = TestBed.get(AllergeneService);
    expect(service).toBeTruthy();
  });
});
