import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './layout/login/login.component';
import { HomeComponent } from './layout/home/home.component';
import { PlatComponent } from './layout/plat/plat.component';
import { UpdatePlatComponent } from './layout/update-plat/update-plat.component';
import { MenuListComponent } from './layout/menu/menu-list/menu-list.component';
import { AddMenuComponent } from './layout/menu/add-menu/add-menu.component';
import { ReservationListComponent } from './layout/reservation/reservation-list/reservation-list.component';
import { ShowReservationComponent } from './layout/reservation/show-reservation/show-reservation.component';
import { AddPlatComponent } from './layout/plats/add-plat/add-plat.component';
import { PrestaireListComponent } from './layout/prestataire/prestataire-list/prestataire-list.component';
import { AddPrestataireComponent } from './layout/prestataire/add-prestataire/add-prestataire.component';
import { GammeListComponent } from './layout/gamme/gamme-list/gamme-list.component';
import { AddGammeComponent } from './layout/gamme/add-gamme/add-gamme.component';
import { ThemeListComponent } from './layout/theme/theme-list/theme-list.component';
import { AddThemeComponent } from './layout/theme/add-theme/add-theme.component';
import { PortionListComponent } from './layout/portion/portion-list/portion-list.component';
import { AddPortionComponent } from './layout/portion/add-portion/add-portion.component';
import { TypePlatListComponent } from './layout/typePlat/type-plat-list/type-plat-list.component';
import { AddTypePlatComponent } from './layout/typePlat/add-type-plat/add-type-plat.component';
import { NutritionListComponent } from './layout/nutrition/nutrition-list/nutrition-list.component';
import { AddNutritionComponent } from './layout/nutrition/add-nutrition/add-nutrition.component';
import { AllergeneListComponent } from './layout/allergene/allergene-list/allergene-list.component';
import { AddAllergeneComponent } from './layout/allergene/add-allergene/add-allergene.component';
import { UpdateThemeComponent } from './layout/theme/update-theme/update-theme.component';
import { UpdateAllergeneComponent } from './layout/allergene/update-allergene/update-allergene.component';
import { UpdateNutritionComponent } from './layout/nutrition/update-nutrition/update-nutrition.component';
import { UpdateGammeComponent } from './layout/gamme/update-gamme/update-gamme.component';
import { UpdatePortionComponent } from './layout/portion/update-portion/update-portion.component';
import { UpdateTypePlatComponent } from './layout/typePlat/update-type-plat/update-type-plat.component';
import { UpdatePrestataireComponent } from './layout/prestataire/update-prestataire/update-prestataire.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },// redirect to `first-component`
  { path: 'plat', component: PlatComponent },
  { path: 'addPlat', component: AddPlatComponent },
  { path: 'updatePlat', component: UpdatePlatComponent },
  { path: 'menu', component: MenuListComponent },
  { path: 'addMenu', component: AddMenuComponent },
  { path: 'reservations', component: ReservationListComponent },
  { path: 'showReservation', component: ShowReservationComponent },
  { path: 'addPlat', component: AddPlatComponent },
  { path: 'prestataire', component: PrestaireListComponent },
  { path: 'addPrestataire', component: AddPrestataireComponent },
  { path: 'updatePrestataire', component: UpdatePrestataireComponent },
  { path: 'gamme', component: GammeListComponent },
  { path: 'addGamme', component: AddGammeComponent },
  { path: 'updateGamme', component: UpdateGammeComponent },
  { path: 'theme', component: ThemeListComponent },
  { path: 'addTheme', component: AddThemeComponent },
  { path: 'updateTheme', component: UpdateThemeComponent },
  { path: 'portion', component: PortionListComponent },
  { path: 'addPortion', component: AddPortionComponent },
  { path: 'updatePortion', component: UpdatePortionComponent },
  { path: 'typePlat', component: TypePlatListComponent },
  { path: 'addTypePlat', component: AddTypePlatComponent },
  { path: 'updateTypePlat', component: UpdateTypePlatComponent },
  { path: 'nutrition', component: NutritionListComponent },
  { path: 'updateNutrition', component: UpdateNutritionComponent },
  { path: 'addNutrition', component: AddNutritionComponent },
  { path: 'allergene', component: AllergeneListComponent },
  { path: 'addAllergene', component: AddAllergeneComponent },
  { path: 'updateAllergene', component: UpdateAllergeneComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
